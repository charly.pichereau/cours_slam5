Framagit

Les Issues correspondent aux tâches, problèmes et discussion en raport avec le projet.
Les Milestones sont des jalons ou des étapes de la  création placé dans le temps.

Différentes commandes :
-'git config --global user.name/email' Modifie l'utilisateur
-'git clone https://framagit.org/...' Récupere le dépot a l'adresse suivante
-'git add fichier' Ajoute le fichier aux modifications
-'git commit -m "nomCommit" ' Fusionne avec les modifications
-'git pull' Met à jour le dépot de git
-'git push' Envoie les modifications au dépot

Gestionnaire de versions

- modification du même fichier sur des lignes != :
	-Fusion sans conflit
	-Le commit réalise la fusion
	-Faire un 'git pull' avant chaque 'git push'

- modification du même chichier sur les même lignes :
	-Fusion conflictuelle
	-'git mergetool' -> 'meld'
	-'git commit'
